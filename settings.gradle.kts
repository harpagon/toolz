dependencyResolutionManagement {
	repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
	repositories {
		google()
		mavenCentral()
	}
}
rootProject.name = "Toolz"
include(":ed")
include(":player")
include(":recorder")
include(":third")
include(":toolz")
include(":fm")
