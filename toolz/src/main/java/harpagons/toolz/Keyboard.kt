package harpagons.toolz

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

object Keyboard {
	private fun imm(ctx: Context) =
		ctx.getSystemService(Context.INPUT_METHOD_SERVICE)
				as InputMethodManager

	fun hide(ctx: Context, view: View) {
		imm(ctx).hideSoftInputFromWindow(
			view.windowToken,
			InputMethodManager.HIDE_IMPLICIT_ONLY
		)
	}

	fun show(ctx: Context, view: View) {
		if (view.requestFocus()) {
			imm(ctx).showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
		}
	}

	fun up(ctx: Context) = imm(ctx).isAcceptingText
}
