package harpagons.toolz

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

fun confirm(ctx: Context, msg: Int, func: () -> Unit) {
	AlertDialog.Builder(ctx)
		.setMessage(msg)
		.setPositiveButton(R.string.yes)
		{ _: DialogInterface, _: Int ->
			func()
		}
		.setNegativeButton(R.string.no)
		{ _: DialogInterface, _: Int -> }
		.show()
}

fun confirmAndExit(ctx: AppCompatActivity, msg: Int, func: () -> Unit) {
	AlertDialog.Builder(ctx)
		.setMessage(msg)
		.setPositiveButton(R.string.yes)
		{ _: DialogInterface, _: Int ->
			func()
			ctx.finish()
		}
		.setNegativeButton(R.string.no)
		{ _: DialogInterface, _: Int ->
			ctx.finish()
		}
		.setNeutralButton(R.string.cancel)
		{ _: DialogInterface, _: Int -> }
		.show()
}
