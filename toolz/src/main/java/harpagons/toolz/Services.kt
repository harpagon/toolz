package harpagons.toolz

import android.app.ActivityManager
import android.app.Service
import android.content.Context
import androidx.appcompat.app.AppCompatActivity

fun isServiceRunning(
	ctx: AppCompatActivity,
	serviceClass: Class<out Service>
): Boolean {
	val m = ctx.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
	for (s in m.getRunningServices(Integer.MAX_VALUE))
		if (serviceClass.name.equals(s.service.className))
			return true
	return false
}
