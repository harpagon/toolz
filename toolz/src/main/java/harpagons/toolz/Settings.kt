package harpagons.toolz

import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

fun launchAppSettings(ctx: AppCompatActivity) {
	val intent = Intent(
		Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
		Uri.parse("package:${ctx.packageName}")
	)
	intent.addCategory(Intent.CATEGORY_DEFAULT)
	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
	ctx.startActivity(intent)
}

fun permissionDialog(ctx: AppCompatActivity) {
	AlertDialog.Builder(ctx)
		.setMessage(R.string.ask_permission)
		.setCancelable(false)
		.setPositiveButton(R.string.app_settings) { _, _ ->
			launchAppSettings(ctx)
			ctx.finish()
		}
		.setNegativeButton(R.string.cancel) { _, _ ->
			ctx.finish()
		}
		.create()
		.show()
}
