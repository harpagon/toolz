package harpagons.toolz

import android.os.Environment
import java.io.File

fun dir(): String = Environment.getExternalStorageDirectory().absolutePath

fun dir(name: String) = "${dir()}/$name"

fun dir(folder: String, name: String) = "${dir()}/$folder/$name"

fun listDir(name: String): List<String> {
	val music = ArrayList<String>()
	File(dir(name)).listFiles()?.forEach { if (it.isFile) music.add(it.name) }
	return music
}

fun readAll(name: String) = File(name).bufferedReader().readText()

fun readNonEmptyLines(name: String): List<String> {
	val s = ArrayList<String>()
	File(name).forEachLine { if (it.isNotEmpty()) s.add(it) }
	return s
}

fun isPlaylist(name: String) = name.substringAfterLast('.', "") == "m3u"
