package harpagons.ed

import android.os.Bundle
import android.view.KeyEvent
import android.view.ViewTreeObserver
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import harpagons.ed.databinding.CmdBinding
import harpagons.toolz.Keyboard
import harpagons.toolz.confirmAndExit
import harpagons.toolz.dir
import java.io.File

class CreateFileActivity : AppCompatActivity() {

	private lateinit var L/*ayout*/: CmdBinding

	private val adjust = ViewTreeObserver.OnGlobalLayoutListener {
		Keyboard.show(this, L.cmdView)
		if (Keyboard.up(this)) removeListener()
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		L = CmdBinding.inflate(layoutInflater)
		L.nameView.setText(R.string.create_file_cmd_name)
		L.cmdView.setOnEditorActionListener { _, action, _ ->
			if (action == EditorInfo.IME_ACTION_DONE) {
				create()
				finish()
			}
			true
		}
		L.parent.viewTreeObserver.addOnGlobalLayoutListener(adjust)
		setContentView(L.root)
	}

	override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
		return when (keyCode) {
			KeyEvent.KEYCODE_BACK -> {
				confirmAndExit(this, R.string.ask_save_file) { create() }
				true
			}
			else -> super.onKeyDown(keyCode, event)
		}
	}

	private fun removeListener() {
		L.parent.viewTreeObserver.removeOnGlobalLayoutListener(adjust)
	}

	private fun create() {
		val name = dir("Documents", L.cmdView.text.toString())
		File(name).createNewFile()
	}
}
