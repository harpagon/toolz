package harpagons.ed

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import harpagons.toolz.permissionDialog

class MainActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		permissionsCheck()
		val name = intent.getStringExtra("name")
		val cls =
			if (name == null) FilesActivity::class.java
			else EdActivity::class.java
		val intent = Intent(this, cls)
		intent.putExtra("name", name)
		startActivity(intent)
		finish()
	}

	private fun permissionsCheck() {
		val permissions = arrayOf(
			Manifest.permission.READ_EXTERNAL_STORAGE,
			Manifest.permission.WRITE_EXTERNAL_STORAGE
		)
		for (permission in permissions)
			if (ContextCompat.checkSelfPermission(
					this,
					permission
				) != PackageManager.PERMISSION_GRANTED
			) permissionDialog(this)
	}
}
