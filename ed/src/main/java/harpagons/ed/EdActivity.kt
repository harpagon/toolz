package harpagons.ed

import android.os.Bundle
import android.view.KeyEvent
import android.view.ViewTreeObserver
import androidx.appcompat.app.AppCompatActivity
import harpagons.ed.databinding.EdBinding
import harpagons.toolz.Keyboard
import harpagons.toolz.confirmAndExit
import harpagons.toolz.dir
import harpagons.toolz.readAll
import java.io.File

class EdActivity : AppCompatActivity() {

	private lateinit var L/*ayout*/: EdBinding
	private lateinit var name: String

	private val adjust = ViewTreeObserver.OnGlobalLayoutListener{
		L.edView.layoutParams.height = L.parent.measuredHeight / 2
		Keyboard.show(this, L.edView)
		if(Keyboard.up(this))  {
			L.edView.setSelection(0)
			removeListener()
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		name = dir("Documents", intent.getStringExtra("name")!!)
		L = EdBinding.inflate(layoutInflater)
		setContentView(L.root)
		L.edView.setText(readAll(name))
		L.parent.viewTreeObserver.addOnGlobalLayoutListener(adjust)
	}

	override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
		return when (keyCode) {
			KeyEvent.KEYCODE_BACK -> {
				confirmAndExit(this, R.string.ask_save_file) {
					File(name).writeText(L.edView.text.toString())
				}
				true
			}
			else -> super.onKeyDown(keyCode, event)
		}
	}

	private fun removeListener() {
		L.parent.viewTreeObserver.removeOnGlobalLayoutListener(adjust)
	}
}
