package harpagons.ed

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import harpagons.ed.databinding.FilesBinding
import harpagons.toolz.confirm
import harpagons.toolz.dir
import harpagons.toolz.listDir
import java.io.File

class FilesActivity : AppCompatActivity() {

	private lateinit var L/*ayout*/: FilesBinding

	private val ls
		get() = ArrayAdapter(
			this, R.layout.entry, listDir("Documents")
		)

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		L = FilesBinding.inflate(layoutInflater)
		L.docListView.setOnItemClickListener { list, _, pos, _ ->
			val intent = Intent(this, EdActivity::class.java)
			val name = list.adapter.getItem(pos) as String
			intent.putExtra("name", name)
			startActivity(intent)
		}
		L.docListView.setOnItemLongClickListener { list, _, pos, _ ->
			confirm(this, R.string.ask_remove) {
				val name = dir("Documents", list.adapter.getItem(pos) as String)
				File(name).delete()
				L.docListView.adapter = ls
			}
			true
		}
		setContentView(L.root)
	}

	override fun onResume() {
		super.onResume()
		L.docListView.adapter = ls
	}

	override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
		return when (keyCode) {
			KeyEvent.KEYCODE_VOLUME_UP -> {
				startActivity(Intent(this, CreateFileActivity::class.java))
				true
			}
			else -> {
				super.onKeyDown(keyCode, event)
			}
		}
	}
}