package harpagons.player

import android.Manifest
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ArrayAdapter
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import harpagons.player.databinding.ActivityMainBinding
import harpagons.toolz.*

class MainActivity : AppCompatActivity() {

	private lateinit var L/*ayout*/: ActivityMainBinding
	private var musicService: MusicService? = null
	private var seekBarHandler: Handler = Handler(Looper.getMainLooper())

	private val seekBarListener = object : SeekBar.OnSeekBarChangeListener {
		override fun onProgressChanged(
			sb: SeekBar, progress: Int, fromUser: Boolean
		) {
			if (fromUser) musicService?.currentPosition = progress
		}

		override fun onStartTrackingTouch(seekBar: SeekBar) = Unit

		override fun onStopTrackingTouch(seekBar: SeekBar) {
			musicService?.currentPosition = seekBar.progress
		}
	}

	private val musicServiceConnection = object : ServiceConnection {
		override fun onServiceConnected(xx: ComponentName, binder: IBinder) {
			musicService = (binder as MusicService.MusicBinder).service
			openPlayer()
		}

		override fun onServiceDisconnected(xx: ComponentName) {
			musicService = null
			closePlayer()
			openList()
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		L = ActivityMainBinding.inflate(layoutInflater)
		setContentView(L.root)
		permissionsCheck()
		execAction()
		L.parentLayout.setOnClickListener { toggleMusicService() }
		BG.set(
			L.parentLayout,
			dir("Pictures", ".player_bg.jpg"),
			resources
		)
	}

	override fun onStart() {
		super.onStart()
		if (isServiceRunning(this, MusicService::class.java)) {
			val intent = Intent(this, MusicService::class.java)
			bindService(intent, musicServiceConnection, 0)
		} else {
			openList()
		}
	}

	override fun onStop() {
		super.onStop()
		if (isServiceRunning(this, MusicService::class.java)) {
			unbindService(musicServiceConnection)
			closePlayer()
		}
	}

	private fun permissionsCheck() {
		val permissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
			arrayOf(
				Manifest.permission.READ_EXTERNAL_STORAGE,
				Manifest.permission.FOREGROUND_SERVICE
			)
		} else {
			arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
		}
		for (permission in permissions)
			if (ContextCompat.checkSelfPermission(
					this,
					permission
				) != PackageManager.PERMISSION_GRANTED
			) permissionDialog(this)
	}

	private fun startMusicService(name: String?) {
		val intent = Intent(this, MusicService::class.java)
		val startService = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
			this::startForegroundService else this::startService
		if (name != null)
			intent.putExtra("name", name)
		if (isServiceRunning(this, MusicService::class.java)) {
			if (musicService != null) unbindService(musicServiceConnection)
			stopService(intent)
		}
		bindService(intent, musicServiceConnection, 0)
		startService(intent)
	}

	private fun execAction() {
		when (intent.action) {
			"STOP" -> stopService(Intent(this, MusicService::class.java))
			"START" -> startMusicService(intent.extras?.getString("name")!!)
			else -> return
		}
		finish()
	}

	private fun seekBarUpdate() {
		L.seekBar.max = musicService?.duration ?: 0
		L.seekBar.progress = musicService?.currentPosition ?: 0
		seekBarHandler.postDelayed(this::seekBarUpdate, 1000)
	}

	private fun openList() {
		initMusicList()
		L.musicListView.visibility = VISIBLE
		L.seekBar.visibility = GONE
	}

	private fun openPlayer() {
		L.musicListView.visibility = GONE
		L.seekBar.visibility = VISIBLE
		L.seekBar.setOnSeekBarChangeListener(seekBarListener)
		seekBarUpdate()
	}

	private fun closePlayer() {
		seekBarHandler.removeCallbacksAndMessages(null)
		L.seekBar.setOnSeekBarChangeListener(null)
	}

	private fun toggleMusicService() {
		if (isServiceRunning(this, MusicService::class.java)) {
			stopService(Intent(this, MusicService::class.java))
			closePlayer()
			openList()
		} else {
			startMusicService(null)
		}
	}

	private fun initMusicList() {
		L.musicListView.adapter = ArrayAdapter(
			this, R.layout.entry, listDir("Music")
		)
		L.musicListView.setOnItemClickListener { list, _, pos, _ ->
			startMusicService(list.adapter.getItem(pos) as String)
		}
	}
}