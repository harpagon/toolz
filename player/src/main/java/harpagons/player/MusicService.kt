package harpagons.player

import android.app.*
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import harpagons.toolz.dir
import harpagons.toolz.isPlaylist
import harpagons.toolz.oneItemList
import harpagons.toolz.readNonEmptyLines

class MusicService : Service() {
	private lateinit var mp: MediaPlayer
	private lateinit var playlist: Playlist
	private var isRunning = false

	val duration: Int
		get() = if (isRunning) mp.duration else 0

	var currentPosition: Int
		get() = if (isRunning) mp.currentPosition else 0
		set(cp) {
			if (isRunning) mp.seekTo(cp)
		}

	private val binder = MusicBinder()

	inner class MusicBinder : Binder() {
		val service get() = this@MusicService
	}

	override fun onBind(intent: Intent): IBinder = binder

	override fun onCreate() {
		super.onCreate()
		playlist = Playlist()
		mp = MediaPlayer()
		mp.setOnPreparedListener { mp.start(); isRunning = true }
		mp.setOnCompletionListener {
			isRunning = false
			playlist.next()
			prepareMP()
		}
	}

	override fun onDestroy() {
		super.onDestroy()
		playlist.save()
		mp.release()
	}

	override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
		startForeground(111, notification())
		when(intent.action) {
			"STOP" -> {
				stopSelf()
				return START_NOT_STICKY
			}
			"NEXT" -> playlist.next()
			"PREV" -> playlist.prev()
			else /* null, "START" */ -> playlist.load(intent)
		}
		prepareMP()
		return START_STICKY
	}

	private fun pIntent(action: String): PendingIntent {
		val i = Intent(this, MusicService::class.java)
		i.action = action
		return PendingIntent.getService(this, 0, i, 0)
	}

	private fun notification(): Notification {
		val v = RemoteViews(packageName, R.layout.notification)
		v.setOnClickPendingIntent(R.id.prevButton, pIntent("PREV"))
		v.setOnClickPendingIntent(R.id.stopButton, pIntent("STOP"))
		v.setOnClickPendingIntent(R.id.nextButton, pIntent("NEXT"))
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			val channel = NotificationChannel(
				"harpagons.player.channel",
				"Harpagon's player",
				NotificationManager.IMPORTANCE_DEFAULT
			)
			channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
			getSystemService(NotificationManager::class.java)
				.createNotificationChannel(channel)
		}
		return NotificationCompat.Builder(this, "harpagons.player.channel")
			.setContentTitle(getString(R.string.app_name))
			.setSmallIcon(R.mipmap.ic_launcher)
			.setContent(v)
			.setCustomBigContentView(v)
			.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
			.build()
	}

	private fun prepareMP() {
		val current = playlist.current
		if (current == null) {
			stopSelf()
		} else {
			mp.reset()
			mp.setDataSource(dir("Music", current))
			mp./*Async*/prepare()
			mp.seekTo(playlist.time)
		}
	}

	private inner class Playlist {
		var name: String? private set
		var tracks: List<String>? private set
		var position: Int
		var time: Int

		val count: Int get() = tracks?.size ?: 0
		val current: String?
			get() = if (position >= count) null else tracks?.get(position)

		private val sharedPreferences
			get() = getSharedPreferences(
				"harpagons.player.service", MODE_PRIVATE
			)

		init {
			val sp = sharedPreferences
			name = sp.getString("name", null)
			position = sp.getInt("position", 0)
			time = sp.getInt("time", 0)
			tracks = null
			loadTracks()
		}

		fun load(intent: Intent) {
			val s = intent.getStringExtra("name")
			if (s != null) {
				name = s
				position = intent.getIntExtra("position", 0)
				time = intent.getIntExtra("time", 0)
				loadTracks()
			}
		}

		fun loadTracks() {
			val n = name
			tracks = when {
				n == null -> null
				isPlaylist(n) -> readNonEmptyLines(dir("Music", n))
				else -> oneItemList(n)
			}
		}

		fun next() {
			if (position < count) {
				position += 1
				time = 0
			}
		}

		fun prev() {
			if (position > 0) {
				position -= 1
				time = 0
			}
		}

		fun save() {
			val spe = sharedPreferences.edit()
			if (current == null) spe.clear()
			else spe
				.putString("name", name)
				.putInt("position", position)
				.putInt("time", mp.currentPosition)
			spe.apply()
		}
	}
}
