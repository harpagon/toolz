# Recorder

A simple application to record rapidly multiple small audio files from
microphone.

## Usage

__REC__ button (gray) - start recording. Note, when it turns red that means
recording started.

__REC__ button (red) - stop recording.

__Volume up__ - bring up / down keyboard for file name prefix edit

__Volume down__ (while inactive) - record while you hold it.

__Volume down__ (while recording) - stop recording for the time you hold it.

__Back__ - blocked to prevent accidental app termination during recording

Note, while using __Volume down__ button, rec button and __Volume up__ button
are blocked. Similarily, when keyboard is up, recording is blocked.

Each time recording starts new file is created.

File name format is: __~/recorded/PREFIX-NUMBER.wav__

Or in case no prefix is given: __~/recorded/NUMBER.wav__

__PREFIX__ is set using text field.

__NUMBER__ number is chosen depending on already existing files.

## Config

App attemps to read __~/Pictures/.recorder_bg.jpg__ file, to set up as
background.

Small personal touch to an interface is always nice I think.

## Possible TODO

* Change Recorder implementation
* Allow recording using different quality and compression settings

## LICENSE

The source of the excelent and eye opening sound recorder can found here:

[RehearsalAduioRecorder](https://sourceforge.net/p/rehearsalassist/code/HEAD/tree/android/branches/uncompressed_recording/src/urbanstew/RehearsalAssistant/RehearsalAudioRecorder.java#l403)

I made a couple small changes/fixes.

It seems to be released under __GPL2.0__, which means this project and compiled
application must also be released under this license, as long as we use this
package.

[GPL2.0](https://www.gnu.org/licenses/old-licenses/lgpl-2.0.html)

If you change recorder implementation, you can use my project under __MIT__
license.

Copyright 2020 Michał Kloc

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## References

[GPL2.0](https://www.gnu.org/licenses/old-licenses/lgpl-2.0.html)

[MIT](https://opensource.org/licenses/MIT)

[RehearsalAduioRecorder](https://sourceforge.net/p/rehearsalassist/code/HEAD/tree/android/branches/uncompressed_recording/src/urbanstew/RehearsalAssistant/RehearsalAudioRecorder.java#l403)

[WAVE PCM soundfile format](http://soundfile.sapp.org/doc/WaveFormat/)

[PCM](https://wiki.multimedia.cx/index.php/PCM)

[Recording .Wav with Android AudioRecorder](https://stackoverflow.com/questions/17192256/recording-wav-with-android-audiorecorder?noredirect=1&lq=1)

[Java vs Kotlin](https://www.kotlinvsjava.com/index.html)

[How to Hide and Show Soft Keyboard in Android](http://www.androidtutorialshub.com/how-to-hide-and-show-soft-keyboard-in-android/)

[Close/hide android soft keyboard](https://stackoverflow.com/questions/1109022/close-hide-android-soft-keyboard)

[Missing autofillHints attribute](https://stackoverflow.com/questions/52690296/missing-autofillhints-attribute)

[Disable automatic input focus gaining on EditText](https://www.android-examples.com/stop-edittext-gaining-focus-at-activity-startup-android)

[Can I delete the folder androidTest if I don't do test in Android studio](https://stackoverflow.com/questions/30564883/can-i-delete-the-folder-androidtest-if-i-dont-do-test-in-android-studio)

