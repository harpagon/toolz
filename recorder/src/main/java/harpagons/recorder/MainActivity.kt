package harpagons.recorder

import android.Manifest
import android.content.pm.PackageManager
import android.media.AudioFormat
import android.media.MediaRecorder
import android.os.Bundle
import android.view.KeyEvent
import android.view.View.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import harpagons.recorder.databinding.ActivityMainBinding
import harpagons.toolz.BG
import harpagons.toolz.Keyboard
import harpagons.toolz.dir
import harpagons.toolz.permissionDialog
import urbanstew.RehearsalAssistant.RehearsalAudioRecorder
import java.io.File
import java.io.IOException


class MainActivity : AppCompatActivity() {

	private lateinit var L/*ayout*/: ActivityMainBinding

	private val destinationDir = dir("Music")
	private val destination = File(destinationDir)
	private var recorder = RehearsalAudioRecorder(
		true,
		MediaRecorder.AudioSource.MIC,
		44100,
		AudioFormat.CHANNEL_IN_MONO,
		AudioFormat.ENCODING_PCM_16BIT
	)
	private var isReversed = false

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		L = ActivityMainBinding.inflate(layoutInflater)
		setContentView(L.root)
		permissionCheck()
		// if this fails, this will be super easy,
		// barely an inconvenience
		if (BG.set(
				L.parentLayout,
				dir("Pictures", ".recorder_bg.jpg"),
				resources
			)
		) {
			L.parentLayout.setOnClickListener { toggleToolzVisible() }
		}
		L.recButton.setOnClickListener { onButtonClick() }
	}

	override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
		when (keyCode) {
			KeyEvent.KEYCODE_VOLUME_DOWN -> {
				if (!(isReversed or L.fileNameEdit.isEnabled)) {
					isReversed = true
					signalRecorder()
					signalButton()
				}
				return true
			}
			KeyEvent.KEYCODE_VOLUME_UP -> {
				if (isReversed or isRecording()) return true
				L.fileNameEdit.isEnabled = !L.fileNameEdit.isEnabled
				if (L.fileNameEdit.isEnabled) {
					L.fileNameEdit.setBackgroundResource(R.color.fileNameBG)
					Keyboard.show(applicationContext, L.fileNameEdit)
				} else {
					L.fileNameEdit.setBackgroundResource(R.color.fileNameTranspBG)
					Keyboard.hide(applicationContext, L.fileNameEdit)
				}
				return true
			}
			KeyEvent.KEYCODE_BACK -> {
				// we block this so we don't accidentally kill the app
				// during recording
				return true
			}
			else -> {
				return super.onKeyDown(keyCode, event)
			}
		}
	}

	override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
		when (keyCode) {
			KeyEvent.KEYCODE_VOLUME_DOWN -> {
				if (isReversed and !L.fileNameEdit.isEnabled) {
					isReversed = false
					signalRecorder()
					signalButton()
				}
				return true
			}
			KeyEvent.KEYCODE_VOLUME_UP -> {
				return true
			}
			KeyEvent.KEYCODE_BACK -> {
				return true
			}
		}
		return super.onKeyUp(keyCode, event)
	}

	private fun permissionCheck() {
		val permissions = arrayOf(
			Manifest.permission.RECORD_AUDIO,
			Manifest.permission.WRITE_EXTERNAL_STORAGE,
			Manifest.permission.READ_EXTERNAL_STORAGE
		)
		for (permission in permissions)
			if (ContextCompat.checkSelfPermission(
					this,
					permission
				) != PackageManager.PERMISSION_GRANTED
			) permissionDialog(this)
	}

	private fun onButtonClick() {
		if (!(isReversed or L.fileNameEdit.isEnabled)) {
			signalRecorder()
			signalButton()
		}
	}

	private fun signalRecorder() {
		try {
			if (isRecording()) {
				recorder.reset()
			} else {
				if (!destination.exists()) {
					destination.mkdir()
				}
				recorder.setOutputFile("$destinationDir/${findAvailableName()}")
				recorder.prepare()
				recorder.start()
			}
		} catch (e: IllegalStateException) {
			e.printStackTrace()
		} catch (e: IOException) {
			e.printStackTrace()
		}
	}

	private fun signalButton() {
		val color: Int =
			if (isRecording()) {
				if (isReversed) {
					R.color.recordingQuick
				} else {
					R.color.recordingActive
				}
			} else {
				if (isReversed) {
					R.color.recordingBreak
				} else {
					R.color.recordingInactive
				}
			}
		L.recButton.setBackgroundColor(
			ResourcesCompat.getColor(
				resources,
				color,
				null
			)
		)
	}

	private fun isRecording(): Boolean {
		return recorder.state == RehearsalAudioRecorder.State.RECORDING
	}

	private fun formName(prefix: String, i: Int): String {
		return if (prefix == "")
			"$i.wav"
		else
			"$prefix-$i.wav"
	}

	private fun alreadyExists(name: String): Boolean {
		destination.walk().forEach {
			if (name == it.name) return true
		}
		return false
	}

	private fun findAvailableName(): String {
		val prefix = L.fileNameEdit.text.toString()
		var i = 1
		var name = formName(prefix, i)
		while (alreadyExists(name)) {
			i += 1
			name = formName(prefix, i)
		}
		return name
	}

	private fun toggleToolzVisible() {
		if (L.recButton.visibility == VISIBLE) {
			L.recButton.visibility = INVISIBLE
			L.fileNameEdit.visibility = INVISIBLE
		} else {
			L.recButton.visibility = VISIBLE
			L.fileNameEdit.visibility = VISIBLE
		}
	}
}
