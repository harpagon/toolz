package harpagons.fm

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import harpagons.fm.databinding.ActivityMainBinding
import harpagons.toolz.listDir
import harpagons.toolz.permissionDialog

class MainActivity : AppCompatActivity() {
	private lateinit var L/*ayout*/: ActivityMainBinding

	private inner class Mode(val path: String, val app: String)

	private val modes = arrayOf(
		Mode("Documents", "harpagons.ed"),
		Mode("Music", "harpagons.player")
		/*Mode("Pictures", "harpagons.imgViewer")*/
	)
	private var mi = -1
	private val mode get() = modes[mi]
	private val ls
		get() = ArrayAdapter(
			this, R.layout.entry, listDir(mode.path)
		)

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		permissionsCheck()
		L = ActivityMainBinding.inflate(layoutInflater)
		setContentView(L.root)
		nextMode()
		L.parent.setOnClickListener { nextMode() }
		L.fileListView.setOnItemClickListener { list, _, pos, _ ->
			startApp(mode.app, list.adapter.getItem(pos) as String)
		}
	}

	private fun permissionsCheck() {
		val permissions = arrayOf(
			Manifest.permission.READ_EXTERNAL_STORAGE,
			Manifest.permission.WRITE_EXTERNAL_STORAGE
		)
		for (permission in permissions)
			if (ContextCompat.checkSelfPermission(
					this,
					permission
				) != PackageManager.PERMISSION_GRANTED
			) permissionDialog(this)
	}

	private fun nextMode() {
		mi = (mi + 1) % modes.size
		L.fileListView.adapter = ls
		L.modeNameView.text = mode.path
	}

	private fun startApp(app: String, name: String): Boolean {
		val intent = packageManager.getLaunchIntentForPackage(app)
		if (intent != null) {
			intent.addCategory(Intent.CATEGORY_DEFAULT)
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
			intent.action = "START"
			intent.putExtra("name", name)
			startActivity(intent)
			return true
		}
		return false
	}
}
