plugins {
	id("com.android.application")
	kotlin("android")
}

android {
	compileSdk = 30

	defaultConfig {
		applicationId = "harpagons.fm"
		minSdk = 16
		targetSdk = 30
		versionCode = 1
		versionName = "1.0"
	}

	buildFeatures {
		viewBinding = true
	}
	buildTypes {
		release {
			isMinifyEnabled = false
			proguardFiles(
				getDefaultProguardFile("proguard-android-optimize.txt")
			)
		}
	}
	compileOptions {
		sourceCompatibility(JavaVersion.VERSION_1_8)
		targetCompatibility(JavaVersion.VERSION_1_8)
	}
	kotlinOptions {
		jvmTarget = "1.8"
	}
}

dependencies {
	implementation("androidx.core:core-ktx:1.3.2")
	implementation("androidx.appcompat:appcompat:1.2.0")
	implementation("com.google.android.material:material:1.3.0")
	implementation("androidx.constraintlayout:constraintlayout:2.0.4")
	implementation(project(":toolz"))
}
